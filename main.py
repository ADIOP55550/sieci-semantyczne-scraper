#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
from typing import *
from pystreamapi import Stream
from stringcase import snakecase
from collections import deque, defaultdict
from neo4j import GraphDatabase
from dotenv import load_dotenv
from flatten_dict import flatten

import bs4
import requests
import re
import threading
import os

BASE_URL = "https://brickset.com"


flat_map = lambda f, xs: [y for ys in xs for y in f(ys)]

from collections.abc import MutableMapping

def flatten_dict(d: MutableMapping, parent_key: str = '', sep: str ='_') -> MutableMapping:
    return flatten(dict(d), reducer="underscore")

def peek(x):
    print(x)
    print(repr(x))
    return x

def get_url(url: str) -> bs4.BeautifulSoup:
    resp = requests.get(url)
    soup = bs4.BeautifulSoup(resp.text, "html.parser")
    return soup

all_sets: Dict[str, 'LegoSet'] = dict()
all_figs: Dict[str, 'Minifig'] = dict()

queue_sets: Deque[str] = deque(maxlen=20)
queue_figs: Deque[str] = deque()

visited_sets: Set[str] = set()
visited_figs: Set[str] = set()

class Neo4JConnection:

    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        self.driver.close()

    def example_method(self, message):
        with self.driver.session() as session:
            greeting = session.execute_write(self._example_task, message)
            print(greeting)

    def drop_everything(self):
        self.driver.execute_query(
            '''
            MATCH (n)
            DETACH DELETE n;
            '''
        )


    def create_minifigure(self, minifig_dict):
        with self.driver.session() as session:
            inserted_set = session.execute_write(self._create_minifigure, minifig_dict)

    @staticmethod
    def _create_minifigure(tx, minifig_dict):
        result = tx.run('''
        CREATE (a:Minifigure {
            name: $name,
            fig_id: $minifig_number,
            character_name: $character_name,
            category: $category,
            year_released: $year_released,
            value: $current_value_new
        })
        RETURN a;
        ''', **minifig_dict)

        sets = minifig_dict.get('appears_in', None)
        if(sets is not None):
            minifig_id = minifig_dict['minifig_number']
            for s in sets:
                tx.run('''
                MATCH (f:Minifigure {fig_id: $minifig_id}), (s:LegoSet {set_id: $set_id})
                MERGE (f)-[:appears_in]->(s)-[:includes]->(f);
                ''', 
                       minifig_id=minifig_id, set_id=s)

        return list(result)


    def create_tag(self, tag_name):
        with self.driver.session() as session:
            inserted_tag = session.execute_write(self._create_tag, tag_name)

    @staticmethod
    def _create_tag(tx, tag_name):
        result = tx.run('''
        CREATE (a:Tag {
            name: $name
        })
        RETURN a;
        ''', name=tag_name)
        return list(result)    


    def create_lego_set(self, set_details_dict):
        with self.driver.session() as session:
            inserted_set = session.execute_write(self._create_lego_set, set_details_dict)

    @staticmethod
    def _create_lego_set(tx, lego_set_dict):
        result = tx.run('''
        CREATE (a:LegoSet {
            name: $name,
            set_id: $set_number,
            theme: $theme,
            year_released: $year_released,
            pieces_count: $pieces,
            minifigs_count: $minifigs_all,
            minifigs_unique: $minifigs_unique,
            value: $current_value_new
        })
        RETURN a;
        ''', **lego_set_dict)

        set_id = lego_set_dict['set_number']
        tags = lego_set_dict.get('tags', [])
        for t in tags:
            tx.run('''
            MATCH (s:LegoSet {set_id:$set_id}), (t:Tag {name: $tag_name})
            MERGE (s)-[:is_tagged]->(t)-[:tags]->(s);
            ''',
                   set_id=set_id, tag_name=t)

        return list(result)

    @staticmethod
    def _example_task(tx, message):
        result = tx.run("CREATE (a:Greeting) "
            "SET a.message = $message "
            "RETURN a.message + ', from node ' + id(a)", message=message)
        return result.single()[0]

class LegoSet:
    name: str
    _set_id: str
    release_date: Union[None, datetime]

    @property
    def set_id(self):
        """The set_id property."""
        return self._set_id

    def __init__(self, name, set_id, release_date):
        self.name = name
        self._set_id = set_id
        self.release_date = release_date

    def __str__(self):
        return f"LegoSet {{ name: '{self.name}' id: {self.set_id} released: {self.release_date} }}"

    def __repr__(self):
        return f"LegoSet({repr(self.name)}, {repr(self.set_id)}, {self.release_date and repr(self.release_date)})"

    @staticmethod
    def map_set_details(details_pair: (str, Union[str, List[str]])) -> (str, Any):
        key = snakecase(details_pair[0])
        values = details_pair[1]
        mapped_to_int = ['year_released', 'pieces']

        if(key == "current_value"):
            if(len(values) == 2): # not known
                values = {'new': None, 'used': None}
            else:
                values = dict(zip(
                    map(lambda s: snakecase(s[:-1]), values[::2]), 
                    map(float , [re.sub("[^0-9\.]", "", x) for x in values[1::2]])
                ))

        if(key == "appears_in_themes"):
            values = [values] if type(values) is not list else values

        if(key == "appears_in"):
            # it's list of sets or number of sets
            if(type(values) is not list and type(values) is not tuple):
                if("set" in values):
                    key = "appears_in_n_sets"
                    values = int(values[:-4]) # strip " sets" from the end
                else:
                    values = [values]

            # Check if it is a list of sets *after* string to list conversion
            if(type(values) is list or type(values) is tuple):
                values = [LegoSet.from_id(x) for x in values]

        if(key == "launch/exit"):
            key = "launch_exit"
            values = list(map(lambda x: None if "t.b.a" in x else datetime.strptime(x, "%d %b %Y"), map(trim_str, values.split('-'))))

        if(key == "minifigs"):
            values = dict(zip(['all','unique'], map(lambda x: int(re.sub("\\D", "", x)), values)))

        if(key in mapped_to_int):
            values = int(values)

        if(key == "tags"):
            if(type(values) == str):
                values = [values]
            else:
                values = values[1:]

        if(key == "rating"): 
            if(type(values) == str):
                values = [values]
            values = [re.sub("[^ a-zA-Z0-9\.,]", "", x) for x in values]
            values = [re.sub("([A-Z])", " \\1", x).split(' ') for x in values if x != '']
            values = [(('score', float(x[0])) if len(x) == 1 else (x[1].lower(), int(x[0]))) for x in values if len(x) <= 2]
            values = dict(values)

        return (key, values)

    @staticmethod
    def get_minifigs_for_set(set_id: str) -> List['Minifig']:
        minifigs_url = BASE_URL + "/minifigs/in-" + set_id

        soup = get_url(minifigs_url)
        all_metas = [x for x in soup.select("article>div.meta")]

        take_first_n_values = 10

        minifigs = []

        for meta in all_metas[:take_first_n_values]:
            title = meta.select_one("h1")
            link = title.select_one("a")
            link_href = link.attrs['href']
            fig_id = link_href.split('/')[-2]
            minifigs.append(fig_id)

        return minifigs

    @staticmethod
    def from_id(id: str) -> 'LegoSet':
        if(id in all_sets):
            return all_sets[id]

        url = BASE_URL + "/sets/" + id
        soup = get_url(url)

        details_section = soup.select_one("section.featurebox")
        details_names = [x.string for x in details_section.select("dt")]
        details_values = (
            Stream.of(details_section.select("dd"))
            # get strings
            .map(lambda x: list(x.strings))
            # trim all elements and remove empty strings
            .map(lambda x: Stream.of(x).map(trim_str).filter(lambda s: len(s) > 0).to_list())
            # Extract string it it is the only element
            .map(lambda x: x[0] if len(x) == 1 else x)
            .to_list()
        )

        minifigs_link = details_section.select_one('a[href^="/minifigs/"]')
        # Get all minifigs from link
        minifigs = LegoSet.get_minifigs_for_set(minifigs_link.attrs['href'].split('/in-')[-1])

        queue_figs.extend(minifigs)


        details_dict = defaultdict(lambda _: "", map(LegoSet.map_set_details, zip(details_names, details_values)))

        set_id = details_dict['set_number']
        lset = flatten_dict(details_dict)
        
        lset.setdefault("pieces", 0)
        lset.setdefault("minifigs_unique", 0)
        lset.setdefault("minifigs_all", 0)
        lset.setdefault("current_value_new", 0)
        lset.setdefault("current_value_used", 0)
        lset.setdefault("theme", "")
        lset.setdefault("year_released", 1970)

        return lset

def trim_str(s: str) -> str:
    return s.strip()

class Minifig:
    name: str
    _fig_id: str
    sets: List[str]
    release_date: Union[None, datetime]

    @property
    def fig_id(self):
        """The fig_id property."""
        return self._fig_id

    def __str__(self):
        return f"Minifig {{ name: '{self.name}' id: {self.fig_id} released: {self.release_date} in_sets: [{self.sets}] }}"

    def __repr__(self):
        return f"Minifig({repr(self.name)}, {repr(self.fig_id)}, {repr(self.release_date)}, {repr(self.sets)})"

    def __init__(self, name, fig_id, release_date, sets):
        self.name = name
        self._fig_id = fig_id
        self.release_date = release_date
        self.sets = sets


    @staticmethod
    def map_minifig_details(details_pair: (str, Union[str, List[str]])) -> (str, Any):
        key = snakecase(details_pair[0])
        values = details_pair[1]

        mapped_to_int = ['year_released']

        if(key == "current_value"):
            if(len(values) == 2): # not known
                values = {'new': None, 'used': None}
            else:
                values = dict(zip(
                    map(lambda s: snakecase(s[:-1]), values[::2]), 
                    map(lambda x: float(re.sub("[^\d\.]", "", x)), values[1::2])
                ))

        if(key == "appears_in_themes"):
            values = [values] if type(values) is not list else values

        if(key == "appears_in"):
            # it's list of sets or number of sets
            if(type(values) is not list and type(values) is not tuple):
                if("set" in values):
                    key = "appears_in_n_sets"
                    values = int(values[:-4]) # strip " sets" from the end
                else:
                    values = [values]

            # Check if it is a list of sets *after* string to list conversion
            if(type(values) is list or type(values) is tuple):
                values = values

        if(key == "tags"):
            if(type(values) == str):
                values = [values]

        if(key in mapped_to_int):
            values = int(values)

        return (key, values)


    @staticmethod
    def from_id(minifig_id: str) -> 'Minifig':
        if(minifig_id in all_figs):
            return all_figs[minifig_id]

        soup = get_url(BASE_URL + "/minifigs/" + minifig_id)
        details_section = soup.select_one("section.featurebox")
        details_names = [x.string for x in details_section.select("dt")]
        # details_values = [list(map(trim_str, lst)) if len(lst) > 1 else trim_str(lst[0]) for lst in [list(x.strings) for x in details_section.select("dd")]]
        details_values = (
            Stream.of(details_section.select("dd"))
            # get strings
            .map(lambda x: list(x.strings))
            # trim all elements and remove empty strings
            .map(lambda x: Stream.of(x).map(trim_str).filter(lambda s: len(s) > 0).to_list())
            # Extract string it it is the only element
            .map(lambda x: x[0] if len(x) == 1 else x)
            .to_list()
        )

        details_dict = defaultdict(lambda _: "", map(Minifig.map_minifig_details, zip(details_names, details_values)))

        fig_id = details_dict['minifig_number']
        sets = details_dict.get('appears_in', [])
        queue_sets.extend(sets)
        fig = flatten_dict(details_dict)
        
        fig.setdefault("current_value_new", 0)
        fig.setdefault("current_value_used", 0)
        fig.setdefault("character_name", "")
        fig.setdefault("category", "")
        fig.setdefault("year_released", 1970)

        return fig



def main():
    query = 'yoda'
    category = 'minifigs'
    URL = f"{BASE_URL}/{category}?query={query}"
    print("Using " + URL)

    load_dotenv()

    soup = get_url(URL)

    all_metas = [x for x in soup.select("article>div.meta")]

    take_first_n_values = 10

    for meta in all_metas[:take_first_n_values]:
        title = meta.select_one("h1")
        link = title.select_one("a")
        link_href = link.attrs['href']

        fig_id = link_href.split('/')[-2]
        queue_figs.append(fig_id)

    print(f"{queue_sets = }")
    print(f"{queue_figs = }")

    def run_download():
        global queue_figs
        global queue_sets
        global all_sets
        global all_figs
        global visited_sets
        global visited_figs

        while (len(queue_figs) + len(queue_sets) > 0):
            print(f"Remaining: {len(queue_sets)} sets, {len(queue_figs)} figs")
            if(len(queue_figs) > 0):
                fig_id = queue_figs.popleft()
                if(fig_id in visited_figs):
                    continue
                visited_figs.add(fig_id)
                print(f"Visiting {fig_id=}")
                fig = Minifig.from_id(fig_id)
                all_figs[fig_id] = fig

            if(len(queue_sets) > 0):
                set_id = queue_sets.popleft()
                if(set_id in visited_sets):
                    continue
                visited_sets.add(set_id)
                print(f"Visiting {set_id=}")
                _set = LegoSet.from_id(set_id)
                all_sets[set_id] = _set


    threads = [threading.Thread(target=run_download) for _ in range(int(os.environ['DOWNLOAD_THREADS']))]

    for t in threads:
        t.start()

    for t in threads:
        t.join()

    all_tags = set()
    for s in all_sets.values():
        if 'tags' not in s:
            continue
        for t in s['tags']:
            all_tags.add(t)

    # print(f"{len(all_sets)=} {all_sets=}\n")
    # print(f"{len(all_figs)=} {all_figs=}\n")
    # print(f"{len(all_tags)=} {all_tags=}\n")

    print(f"Got {len(all_sets)} sets, {len(all_figs)} minifigures, {len(all_tags)} tags")

    neo4j = Neo4JConnection(os.environ['NEO4J_URL'], os.environ['NEO4J_USERNAME'], os.environ['NEO4J_PASSWORD'])

    neo4j.drop_everything()

    for t in all_tags:
        neo4j.create_tag(t)

    for s in all_sets.values():
        neo4j.create_lego_set(s)

    for m in all_figs.values():
        neo4j.create_minifigure(m)

    neo4j.close()


if __name__=="__main__":
    main()

